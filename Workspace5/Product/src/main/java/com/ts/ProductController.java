package com.ts;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDao;
import com.model.Product;

@RestController
public class ProductController {
	
        //Implementing Dependency Injection for ProductDao 
	@Autowired
	ProductDao productDao;
	
	@GetMapping("getAllProducts")
	public List<Product> getAllProducts() {		
		return productDao.getAllProducts();
	}	
	@GetMapping("getProductById/{productId}")
	public Product getProductById(@PathVariable("productId") int productId) {
		return productDao.getProductById(productId);	
	}
	@GetMapping("getProductByName/{productName}")
	public List<Product> getProductByName(@PathVariable("productName") String productName) {
		return productDao.getProductByName(productName);
	}
	@PostMapping("addProduct")
	public Product addProduct(@RequestBody Product product) {
		return productDao.addProduct(product);
	}
	@PutMapping("updateProductById/{productId}")
    public Product updateProductById(@PathVariable("productId") int productId, @RequestBody Product updatedProduct) {
        return productDao.updateProductById(productId, updatedProduct);
    }
	@DeleteMapping("/deleteProductById/{productId}")
	public ResponseEntity<String> deleteProductById(@PathVariable("productId") int productId) {
	    boolean deleted = productDao.deleteProductById(productId);

	    if (deleted) {
	        return new ResponseEntity<>("Product with ID " + productId + " has been deleted", HttpStatus.OK);
	    } else {
	        return new ResponseEntity<>("Product with ID " + productId + " not found", HttpStatus.NOT_FOUND);
	    }
	}
}