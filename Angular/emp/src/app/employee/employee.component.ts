import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrl: './employee.component.css'
})
export class EmployeeComponent implements OnInit {
Employees : any ;

 constructor(){
  this.Employees=[
   { empId : 1 , empName : "Priyanka" , salary : 45000 , gender : "Female" , doj : "2020-05-01" , country : "India" , emailId : "priyanka@gmail.com" , password : "priya" },
   { empId : 2 , empName : "meghana" , salary : 65000 , gender : "Female" , doj : "2020-05-01" , country : "India" , emailId : "meghana@gmail.com" , password : "meghana" },
   { empId : 3 , empName : "suman" , salary : 50000 , gender : "Male" , doj : "2020-05-01" , country : "pakistan" , emailId : "yedava@gmail.com" , password : "420" },
   { empId : 4 , empName : "rohith" , salary : 45000 , gender : "Male" , doj : "2020-05-01" , country : "Zimbambe" , emailId : "ghathana@gmail.com" , password : "521" },
   { empId : 5 , empName : "satya" , salary : 60000 , gender : "Female" , doj : "2020-05-01" , country : "India" , emailId : "satya@gmail.com" , password : "satya" },
  ]
  }
ngOnInit(){

}
}
