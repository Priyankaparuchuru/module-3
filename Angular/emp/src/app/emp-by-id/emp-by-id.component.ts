import { Component ,OnInit } from '@angular/core';

@Component({
  selector: 'app-emp-by-id',
  templateUrl: './emp-by-id.component.html',
  styleUrl: './emp-by-id.component.css'
})
export class EmpByIdComponent implements OnInit {
  employee : any ;
  emp :any;
  employees:any;
  empId:any;

 constructor(){
  this.employees=[
   { empId : 1 , empName : "Priyanka" , salary : 45000 , gender : "Female" , doj : "06/01/2023" , country : "India" , emailId : "priyanka@gmail.com" , password : "priya" },
   { empId : 2 , empName : "meghana" , salary : 65000 , gender : "Female" , doj : "17/02/2023" , country : "India" , emailId : "meghana@gmail.com" , password : "meghana" },
   { empId : 3 , empName : "suman" , salary : 50000 , gender : "male" , doj : "18/02/2023" , country : "pakistan" , emailId : "yedava@gmail.com" , password : "420" },
   { empId : 4 , empName : "rohith" , salary : 45000 , gender : "male" , doj : "20/02/2023" , country : "Zimbambe" , emailId : "ghathana@gmail.com" , password : "521" },
   { empId : 5 , empName : "satya" , salary : 60000 , gender : "Female" , doj : "10/02/2023" , country : "India" , emailId : "satya@gmail.com" , password : "satya" },
  ]
  }
ngOnInit(){

}

getEmployee() {



  this.employees.forEach((employee: any) => {
    if (employee.empId == this.empId) {
      this.emp = employee;
    }
  });
}
}
