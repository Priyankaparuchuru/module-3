import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {
  
  id:number;
  name:string;
  avg:number;
  address : any;
  hobbies :any;

  constructor(){
   // alert("constructor invoked.....");
   this.id=101;
   this.name="priyanka";
   this.avg =25;
   this.address ={
    streetno : 102 ,
    lane : 'lalapeta' ,
     state : 'a.p'};
  
  this.hobbies = ['sleeping','watching movies','exploring'];
   }
   
 ngOnInit(){
 }
}
