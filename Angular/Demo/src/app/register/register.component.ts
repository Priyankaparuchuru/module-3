// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrl: './register.component.css'
// })
// export class RegisterComponent implements OnInit {
  
//   empName: any;
//   salary: any;
//   gender: any;
//   doj: any;
//   country: any;
//   emailId: any;
//   password: any;
  
//   constructor() {
//   }

//   ngOnInit() {
//   }

//   RegisterSubmit(regForm :any) {
//     console.log("EmpName: " + regForm.empName);
//     console.log("Salary: " + regForm.salary);
//     console.log("Gender: " + regForm.gender);
//     console.log("DateOfJoin: " + regForm.doj);
//     console.log("Country: " + regForm.country);
//     console.log("Email-Id: " + regForm.emailId);
//     console.log("Password: " + regForm.password);
//   }

// }
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EmpService } from '../emp.service';
import { FormGroup } from 'reactstrap';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
// export class RegisterComponent implements OnInit {

//   empName: any;
//   salary: any;
//   gender: any;
//   doj: any;
  // country: any;
//   phoneNumber: any;
//   emailId: any;
//   password: any;
//   countries:any;

//   constructor(private router: Router, private toastr: ToastrService,private service: EmpService) {
//   }
  
//   ngOnInit() {
//     this.service.getAllCountries().subscribe((data: any) => {
//       this.countries = data;
//       console.log(data);
//     });
//   }

 
//   submit() {
//     console.log("EmpName: " + this.empName);
//     console.log("Salary: " + this.salary);
//     console.log("Gender: " + this.gender);
//     console.log("DateOfJoin: " + this.doj);
//     // console.log("Country: " + this.country);
//     console.log("PhoneNumber: " + this.phoneNumber);
//     console.log("Email-Id: " + this.emailId);
//     console.log("Password: " + this.password);
//   }

//   registerSubmit() {

//     if (!this.validateForm()) {
//       return;
//     }
//     const newEmployee = {
//       empId: this.generateEmpId(),
//       empName: this.empName,
//       salary: this.salary,
//       gender: this.gender,
//       doj: this.doj,
//       country: this.countries,
//       phoneNumber: this.phoneNumber,
//       emailId: this.emailId,
//       password: this.password
//     };

//     console.log(newEmployee);

//     this.toastr.success('Registration Successful!', 'Success', {
//       closeButton: true,
//       progressBar: true,
//       positionClass: 'toast-top-right',
//       tapToDismiss: false,
//       timeOut: 3000, // 3 seconds
//     });

//     this.router.navigate(['login']);
//   }

//   private validateForm(): boolean {

//     if (!this.empName || !this.salary || !this.emailId || !this.password) {
//       this.toastr.error('Please fill in all required fields.', 'Error', {
//         closeButton: true,
//         progressBar: true,
//         positionClass: 'toast-top-right',
//         tapToDismiss: false,
//         timeOut: 3000, // 3 seconds
//       });
//       return false;
//     }
//     return true;
//   }

//   private generateEmpId(): number {
//     return Math.floor(Math.random() * 1000) + 1;
//   }
// }

export class RegisterComponent implements OnInit {
  protected aFormGroup! : FormGroup;
   
  emp:any;
  empName: any;
  salary: any;
  gender: any;
  doj: any;
  country: any;
  phoneNumber: any;
  emailId: any;
  password: any;
  confirmPassword:any;
  countries:any;
  departments: any;
  siteKey:string = "6Le1mmgpAAAAAG7i2p2-tYWDww-9FgeVYOaY4d1V" ;

  constructor(private router: Router, private toastr: ToastrService,private Service:EmpService, private formBuilder: FormBuilder) {
    this.emp = {
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',
      department: {
        deptId: ''
      }
    };
    
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
    });
  }


  ngOnInit() {
    this.Service.getAllCountries().subscribe((data:any)=>{this.countries=data;});
    this.Service.getAllDepartments().subscribe((data: any) => {this.departments = data;});
    
  }
  registerSubmit(regForm: any) {
    if (this.emp.password !== this.confirmPassword) {
      console.log('Password and Confirm Password must be the same.');
    }
  console.log(regForm);
    if (!this.validateForm()) {
      return;
    }
    this.emp.empName = regForm.empName;
    this.emp.salary = regForm.salary;
    this.emp.gender = regForm.gender;
    this.emp.doj = regForm.doj;
    this.emp.country = regForm.country;
    this.emp.emailId = regForm.emailId;
    this.emp.password = regForm.password;
    this.emp.department.deptId = regForm.department;

    console.log(this.emp);
    

    this.toastr.success('Registration Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000, // 3 seconds
    });

    this.Service. regsiterEmployee(this.emp).subscribe((data: any) => { console.log(data); });
    this.router.navigate(['login']);
    
  }

  private validateForm(): boolean {

    if (!this.empName || !this.salary || !this.emailId || !this.password) {
      this.toastr.error('Please fill in all required fields.', 'Error', {
        closeButton: true,
        progressBar: true,
        positionClass: 'toast-top-right',
        tapToDismiss: false,
        timeOut: 3000, // 3 seconds
      });
      return false;
    }
    return true;
  }

  private generateEmpId(): number {
    return Math.floor(Math.random() * 1000) + 1;
  }
}