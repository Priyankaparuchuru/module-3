import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
//1. Declare jQuery Variable for Modal Dialog Launch
declare var jQuery: any;

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrl: './employee.component.css'
})
export class EmployeeComponent implements OnInit {
  employees: any;
  emailId: any;
  countries: any;
  departments: any;
  editEmp: any;       //for 2-way databinding with dialog box

  constructor(private service: EmpService) {
    this.emailId = localStorage.getItem('emailId');

    //for 2-way databinding with dialog box
    this.editEmp = {
      empId: '',
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',
      department: {
        deptId: ''
      }
    };
  }

  ngOnInit() {
    this.service.getAllEmployees().subscribe((data: any) => { this.employees = data; });
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
    this.service.getAllDepartments().subscribe((data: any) => { this.departments = data; });
  }

  editEmployee(emp: any) {
    console.log(emp);

    //Employee data binding to the editEmp variable for 2-way databinding
    this.editEmp = emp;

    //Launching the Modal Dialog Box
    jQuery('#myModal').modal('show');
  }

  updateEmployee() {
    console.log(this.editEmp);
    this.service.updateEmployee(this.editEmp).subscribe((data: any) => { console.log(data); });
  }


  deleteEmployee(emp: any) {
    this.service.deleteEmployee(emp.empId).subscribe((data: any) => {console.log(data);});

    const i = this.employees.findIndex((element: any) => {
      return element.empId == emp.empId;
    });

    this.employees.splice(i, 1);

    alert('Employee Deleted Successfully!!!');
  }
}
  
    
//  constructor(private service: EmpService){
  //Getting emailId from LocalStorage
  // this.emailId = localStorage.getItem('emailId');

  // this.employees = [
  //   {empId:1, empName:'Meghana', salary:50000.00, gender:'Female', doj:'2023-08-18', country:'INDIA', emailId:'meghana@gmail.com', password:'meghana@123'},
  //   {empId:2, empName:'Priyanka', salary:55000.00, gender:'Female', doj:'2021-08-11', country:'INDIA', emailId:'priyanka@gmail.com', password:'pichi@589'},
  //   {empId:3, empName:'Satya', salary:58000.00, gender:'Female', doj:'2018-05-25', country:'INDIA', emailId:'satya@gmail.com', password:'pilla@517'},
  //   {empId:4, empName:'Suman', salary:36000.00, gender:'Male', doj:'2020-07-01', country:'INDIA', emailId:'suman@gmail.com', password:'yedava@420'},
  //   {empId:5, empName:'Rohith', salary:35000.00, gender:'Male', doj:'2020-06-01', country:'INDIA', emailId:'rohith@gmail.com', password:'ghathana@521'}
  // ]
   
 //}
//  ngOnInit(){
//   this.service.getAllEmployees().subscribe((data: any) => {
//     console.log(data);
//     this.employees = data;
//   });
// }
// submit(){
//   console.log(this.employees)
// }

//   ngOnInit() {
//     this.service.getAllEmployees().subscribe((data: any) => {
//       console.log(data);
//       this.employees = data;
//     });
//   }

//   editEmployee(emp: any) {
//     console.log(emp);

//     //2. Launching the Modal Dialog Box
//     jQuery('#myModal').modal('show');

//   }


//   deleteEmployee(emp: any) {
//     this.service.deleteEmployee(emp.empId).subscribe((data: any) => {console.log(data);});

//     const i = this.employees.findIndex((element: any) => {
//       return element.empId = emp.empId;
//     });

//     this.employees.splice(i, 1);

//     alert('Employee Deleted Successfully!!!');
//   }
// }

