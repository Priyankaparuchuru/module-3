import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EmpService {
  loginstatus: any;
  cartItems: any;
  isUserLoggedIn: any;

  constructor(private http:HttpClient) { 
    this.cartItems = [];

    this.loginstatus = false;
    this.isUserLoggedIn = new Subject();

  }
  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }
  getAllEmployees():any{
    return this.http.get('http://localhost:8085/getEmployees')
  }
  getAllDepartments(): any {
    return this.http.get('http://localhost:8085/getDepartments');
  }
  regsiterEmployee(employee: any): any {
    return this.http.post('http://localhost:8085/addEmployee', employee);
  }
  employeeLogin(emailId: any, password: any): any {
    return this.http.get('http://localhost:8085/empLogin/' + emailId + '/' + password).toPromise();
  }
  deleteEmployee(empId: any) {
    return this.http.delete('http://localhost:8085/deleteEmployeeById/' + empId);
  }
  updateEmployee(employee: any) {
    return this.http.put('http://localhost:8085/updateEmployee', employee);
  }

  //Login
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }

  //Logout
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
  }
  getEmployeeById(empId: any): any {
    return this.http.get('http://localhost:8085/getEmployeeById/' + empId);
  }
  
}